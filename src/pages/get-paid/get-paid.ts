import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import {ToastController} from 'ionic-angular';
// import {HomePage} from '../home/home';

/**
 * Generated class for the GetPaidPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-get-paid',
  templateUrl: 'get-paid.html',
})
export class GetPaidPage {


bank : any = {}

constructor(public navCtrl : NavController, public navParams : NavParams, public storage : Storage, public http : Http, public toastCtrl : ToastController) {

this
  .storage
  .get('bank')
  .then((val) => {
    this.bank = val
    console.log('Your bank is', this.bank);
  });

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad GetPaidPage');
  }



payme(){
  console.log(this.bank.amount);
console.log(this.bank);
}

}
