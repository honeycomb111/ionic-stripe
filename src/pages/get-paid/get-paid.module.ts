import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetPaidPage } from './get-paid';

@NgModule({
  declarations: [
    GetPaidPage,
  ],
  imports: [
    IonicPageModule.forChild(GetPaidPage),
  ],
})
export class GetPaidPageModule {}
