import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {NgClass} from '@angular/common';
import {Storage} from '@ionic/storage';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

import {ToastController} from 'ionic-angular';

import {GetPaidPage} from '../get-paid/get-paid';

@Component({selector: 'page-home', templateUrl: 'home.html'})
export class HomePage {

  bank : any = {
    name: '',
    route: '',
    acocunt_number: '',
    confirm_acocunt_number: '',
    city: ''
  };

  constructor(public navCtrl : NavController, public navParams : NavParams, public storage : Storage, public http : Http, public toastCtrl : ToastController) {
    storage.set('bank', this.bank);
  }

  setupbank() {
    console.log(this.bank)
    this.presentToast(this.bank.name);
    this.goToPage(GetPaidPage);
    this.setStorage('bank', this.bank);
  }

  goToPage(page) {
    this
      .navCtrl
      .push(page);
  }

  presentToast(meg) {
    let toast = this
      .toastCtrl
      .create({message: meg, duration: 3000});
    toast.present();
  }

  setStorage(key, value) {
    this
      .storage
      .set(key, value);
  }

}
