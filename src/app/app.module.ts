import {BrowserModule} from '@angular/platform-browser';
import {HttpModule} from '@angular/http';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {IonicStorageModule} from '@ionic/storage';
import {Toast} from '@ionic-native/toast';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {GetPaidPage} from '../pages/get-paid/get-paid';

@NgModule({
  declarations: [
    MyApp, HomePage, GetPaidPage
  ],
  imports: [
BrowserModule,
HttpModule,
IonicModule.forRoot(MyApp),
IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp, HomePage, GetPaidPage
  ],
providers : [
  StatusBar,
  Toast,
  SplashScreen, {
    provide: ErrorHandler,
    useClass: IonicErrorHandler
  }
]
})
export class AppModule {}